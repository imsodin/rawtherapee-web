---
title: "Download page revised"
date: 2010-10-16T12:27:00+00:00
author: DrSlony
Tags:
- Deprecated Content
- Downloads
---



The [Download](http://rawtherapee.com/?mitem=3 "Download section")
section has been revised. It should now fully and comprehendibly explain
to a new user where RawTherapee stands, and make clear what the
advantages and disadvantages of using the latest stable but outdated
version (2.4.1) or the latest bleeding edge builds are. If you feel that
something is missing, worded in an unclear way, or you have a
suggestion, please tell us about it in the [General
Discussion](http://rawtherapee.com/forum/viewforum.php?f=1 "General Discussion forum")
forum.

