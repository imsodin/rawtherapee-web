---
title: "RawTherapee v2.2 released"
date: 2007-08-24T00:00:00+00:00
author: dualon
Tags:
- RT 2.2
---


Changelog:

  - ICC based color management (with selectable
    input/working/output/monitor profiles)
  - Clipped highlight/shadow indication
  - Reset button for the adjusters
  - Browser shows basic exif data
  - (Last Photo) option in the profile switcher
  - rt.exe can now be called with an image file in command line argument
  - New color scheme, splash screen, icon and home page (thanks, bytec)
  - Fix for the magenta highlights of Olympus cameras
  - Many bug fixes (including file sorting)

If you tried one of the beta versions please
don't forget to delete the "options" file (there is one in the
.RawTherapee folder in your home in the application data directory as
well) and the .pp2 files produced by the beta versions.

Note that the EAHD algorithm introduced recently in
[ufraw](http://ufraw.sourceforge.net/) is not the same as the one in
RawTherapee.

If you like RawTherapee please do not forget about the donation (it is
only $5\!).  
RawTherapee V2.1.1 has been downloaded more than 35000 times and I got
only 80 donations (thank you for them\!).  
Is this software that bad? 34920 people downloaded it and did not find
it usable? In this case please give me some feedback how it could be
improved through the forums.

