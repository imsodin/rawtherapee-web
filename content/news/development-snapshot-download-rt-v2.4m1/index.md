---
title: "Development snapshot: Download RT v2.4m1"
date: 2008-04-23T12:05:00+00:00
author: dualon
Tags:
- RT 2.4
---



**Explanation about 'Development snapshot' Releases:**


This release is not a final product, it is something between a
'Developer Snapshot', a 'Milestone Release', a 'Beta Version' and a
'Release Candidate'. Therefore it could contain features that are not
yet completely implemented and could also contain some more bugs as a
STABLE release.


The idea behind this Releases is to give users a first idea about the
features and get first feedback from the users about still existing bugs
in the product.  


**Requirements:**

  - a fast processor with SSE support is recommended (but not required)
  - at least 512 MB of RAM.
  - Windows version: Windows 2000, XP or Vista (32bit) is required
  - for the linux version: gtk+ 2.12 series required

**Download:**

|                                      |                                                                       |
| ------------------------------------ | --------------------------------------------------------------------- |
| Windows version (SSE support)        | [rawtherapee24m1.exe](http://www.rawtherapee.com/rawtherapee24m1.exe) |
| Tgz archive for linux with glibc 2.4 | [rawtherapee24m1.tgz](http://www.rawtherapee.com/rawtherapee24m1.tgz) |


**Price:**


You can decide how much you pay for RawTherapee.  
It can be downloaded and evaluated for free. If you are satisfied with
it and you can afford an $5 payment, the author kindly asks you to make
a donation to support the further development. Please click on one of
the donation buttons on the right side and make PayPal payment.  


**Feedback:**


If you found a bug or have nice ideas, please visit our
[forum](http://rawtherapee.com/forum/ "RawTherapee Forum") to discuss
them with other users and the author.


**Disclaimer:**
  
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

