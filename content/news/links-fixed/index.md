---
title: "Links fixed"
date: 2014-01-20T06:52:00+00:00
author: DrSlony
Tags:
- Documentation
- Downloads
---



The download link for the 4.0.12 build of RawTherapee for Windows and
the 4.0.12 PDF have been fixed.

