---
title: "RawTherapee.com v3 - The New Homepage!"
date: 2011-05-15T15:35:00+00:00
author: dualon
Tags:
- Community
- Management
- Website
---



Welcome to the new homepage of RawTherapee\!  
  
Rawtherapee.com has evolved from a few simple pages through a basic
dynamic site to the current, advanced homepage. This new site was
carefully planned and designed to be a real home of RawTherapee.  
  


<figure>
<a href="rthp_v1.png"><img src="rthp_v1-177x.png" alt="RawTherapee.com v2"></a>
<a href="rthp_v2.png"><img src="rthp_v2-177x.png" alt="RawTherapee.com v2"></a>
<a href="rthp_v3.png"><img src="rthp_v3-177x.png" alt="RawTherapee.com v3"></a>
</figure>

Versions of the homepage from the beginning

  
Most of the changes are under the hood, but you may have already noticed
[the blog](./blog) and hopefully will soon discover the new, powerful
[download center](./downloads) as well.  
The new site provides a much more efficient<span id="lead_end"></span>
environment for multiple administrators. It helps the
[OSMC](./blog/list/6) to inform you better and to provide regular
updates and downloads.  
  
The previous articles are all transfered to our new database, but there
are many contents to be uploaded and many small glitches to be ironed
out (the site is a 'public beta').  
If you find a bug or have a good idea, please post it in the [issue
tracker](http://code.google.com/p/rawtherapee/issues/list) with the
label 'Website'. Please feel free to join the discussion about the new
homepage in the appropriate [forum
topic](http://rawtherapee.com/forum/viewtopic.php?t=2876). All the
feedbacks are highly appreciated.  
  
I'm honored to be the designer and creator of the new site. More than
22.000 lines of code work as the heart of the custom content management
system which is a robust, high performance, flexible and extensible
framework for our future plans. I'm grateful to all who helped me with
advices, especially to DrSlony, Hombre and paul.matthijsse.  
  
The journey is just about to start. Welcome to the new homepage\!  

