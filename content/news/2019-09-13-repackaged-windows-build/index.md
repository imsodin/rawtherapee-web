---
title: "Repackaged Windows Build"
date: 2019-09-13T19:15:59+02:00
author: DrSlony
draft: false
---

Please download the Windows 5.7 build again, and re-install it.

The original Windows build of RawTherapee 5.7 contained an error in the way it was compiled and packaged, causing it to behave the way our development builds behave - the default installation folder was of the development build form, and it looked for settings in the location where a development build would store them. The error was corrected, and the build has now been replaced with a new one which behaves the way a stable release should behave.

Our apologies for the inconvenience.

sha256sum:  
`f6d78951a5a398e259637ff8c3dadd5209bf6bde6694a3a3e168ba8bfd576316  RawTherapee_5.7_WinVista_64.zip`
