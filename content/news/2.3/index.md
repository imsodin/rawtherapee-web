---
title: "RawTherapee v2.3 released"
date: 2008-02-12T09:45:00+00:00
author: dualon
draft: false
version: ""
linux_appimage: ""
windows_binary: ""
mac_binary: ""
source_code: ""
---


We are proud to present RawTherapee version 2.3\!

Changelog:

  - Image resizing tool
  - Vignetting correction tool
  - Local contrast enhancement tool
  - Auto Crop and Fill option in the rotation tool
  - Multi-language support (already included in package: English,
    German, Hungarian, Latvian, Spanish, Italian, Dutch, Swedish,
    Russian, Polish, Czech, Slovak)
  - Channel mixer
  - Deconvolution based sharpening
  - VNG-4 interpolation of dcraw to reduce maze on Olympus raw images
  - Selectable guide lines when cropping
  - Enhanced auto and spot white balance
  - Support for new cameras (DCRaw 8.82/1.398)
  - Windows Vista support

Besides the new release of the RawTherapee
there are two more surprises:

  - A beautiful new web-page is up and running\! It's made by Bytec with
    user-oriented solutions and supportive areas in mind. The new site
    helps to overview, download, and learn RawTherapee in a centralized,
    efficient way. News are provided via
    [RSS](http://www.rawtherapee.com/rss.php) feed, and there is a tool
    to [compare](http://www.rawtherapee.com/RAW_Compare/) RawTherapee
    with many other converters. We hope you'll enjoy the new
    "headquarters", please feel free to feedback us in the forums\!
  - A development team is introduced, the members are Gábor, Bytec,
    Dualon, Keenonkites and Klonk.

The multi-language support of v2.3 lets you to contribute in
translation. For instructions or other questions check the [FAQ
section](?mitem=4).

For feature requests, bug reports, unanswered questions or for just a
discussion about RawTherapee use our
[forum](http://www.rawtherapee.com/forum/index.php).

Most importantly: have a good time with RawTherapee 2.3\!

Gábor
