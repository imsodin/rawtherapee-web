---
title: "Get The Latest Releases"
date: 2010-10-16T12:09:00+00:00
author: DrSlony
Tags:
- Deprecated Content
- Nightly Builds
---



The latest official release is alpha1 from January. A lot has changed
since then. The "[release early, release
often](http://en.wikipedia.org/wiki/Release_early,_release_often)" model
is planned to be followed to satisfy you with new builds and to
appreciate and show the efforts of the developers. Therefore some new
builds are provided to play with until the bugs (program errors) are
ironed out and the next "official" release can be published.

The latest releases are available in this directory and are updated
regularly:  
<http://www.rawtherapee.com/shared/builds/>

The current builds<span id="lead_end"></span> from this directory:

  - [Windows 32
    bit](http://www.rawtherapee.com/shared/builds/rawtherapee_win32_std_2010-10-15_release-3.0-a1+292-4e6cdd61de02.7z)
    (.7z - you may need [7zip](http://www.7-zip.org/))
  - [Windows 64 bit + SSE
    support](http://www.rawtherapee.com/shared/builds/rawtherapee_win64_corei7sse42_2010-09-17_release-3.0-a1+276-d78bce58e762.7z)
    (.7z)
  - [Mac OS X 10.5 (32
    bit)](http://www.rawtherapee.com/shared/builds/rawtherapee_osx105_2010-10-06_release-3.0-a1+278-09f6415272e0.dmg)
    (.dmg)
  - [Linux 32
    bit](http://www.rawtherapee.com/shared/builds/rawtherapee_linux32_2010-10-14_release-3.0-a1+289-48d4cef8f1c5.tar.bz2)
    (.tar.bz2)
  - [Linux 64
    bit](http://www.rawtherapee.com/shared/builds/rawtherapee_linux64_2010-10-15_release-3.0-a1+293-8e7ead3a466b.tar.bz2)
    (.tar.bz2)

The source code is also available as always, you can [check out via
Mercurial](http://code.google.com/p/rawtherapee/wiki/MercurialHowto).

Please note that:

  - the recent builds are compiled from the head (the main repository of
    RawTherapee, see above the source code),
  - usually are stable enough to work with but not suggested for
    production use,
  - let you check the "bleeding edge" development of RawTherapee and
    therefore may include some bugs.

The known bugs are [listed
here](http://code.google.com/p/rawtherapee/issues/list?can=2&q=&sort=-id&colspec=ID%20Type%20Status%20Priority%20Milestone%20Owner%20Summary).

Bugs can be reported via the Google Code Issue Tracker (please check for
duplicates before you file an issue):  
[RawTherapee Google Code Home](http://code.google.com/p/rawtherapee/)
-\> [Issues](http://code.google.com/p/rawtherapee/issues/list) -\> New
Issue on the upper left side.

Please feel free to use the [forum](../forum/) in case of further
questions, the topic of the latest builds [can be found
here](http://www.rawtherapee.com/forum/viewtopic.php?t=2101).

