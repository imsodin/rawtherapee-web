---
title: "Documentation for Raw Therapee v2.3"
date: 2008-05-28T20:49:00+00:00
author: dualon
Tags:
- RT 2.3
---



If you want to understand how to work with Raw Therapee even better, we
recommend to read the users manual. You can download it in PDF format:

  - [Users Manual for RT v2.3
    (english)](http://www.rawtherapee.com/data/RAWTherapeeManual_2.3.pdf "English translation of the Manual")
  - [Manual de usuario RT v2.3
    (espanol)](http://www.rawtherapee.com/data/RAWTherapeeManual_2.3_es.pdf "Spanish translation of the Manual")
  - [Manuel pour RT v2.3
    (french)](http://www.rawtherapee.com/data/RAWTherapeeManuel_2.3_fr.pdf "French translation of the Manual")  
  - [Benutzerhandbuch für RT v2.3
    (german)](http://www.rawtherapee.com/data/RAWTherapeeHandbuch_2.3.pdf "German translation of the Manual")
  - [Manuale RT v2.3
    (italian)](http://www.rawtherapee.com/data/RAWTherapeeManuale_2.3_it.pdf "Italian Translation of the Manual")

Here are some additonal<span id="lead_end"></span> informations:

  - [Internal Workflow of RT v2.3
    (english)](http://www.rawtherapee.com/data/RT-Internal-Workflow_2.3.pdf "English translation of the internal Workflow Diagram")
  - [Plan de Proceso Interno de RT v2.3
    (espanol)](http://www.rawtherapee.com/data/RT-Plan-De-Proceso-Interno_2.3_es.pdf "Spanish translation of the internal Workflow Diagram")  
  - [Flux de travail Interne de RT v2.3
    (french)](http://www.rawtherapee.com/data/RT-Flux%20de%20travail%20Interne_2.3_fr.pdf "French translation of the internal Workflow Diagram")  
  - [Interner Arbeitsablauf von RT v2.3
    (german)](http://www.rawtherapee.com/data/RT-Interner-Arbeitsablauf_2.3.pdf "German translation of the internal Workflow Diagram")
  - [Modalità operativa interna di RT 2.3
    (italian)](http://www.rawtherapee.com/data/RT-Internal-Workflow_2.3_it.pdf "Italian translation of the internal Workflow Diagram")

For those who want to translate the english manual into their own
language, here the OpenOffice source of the manual and the internal
workflow:

  - [Users Manual for RT v2.3 (english -
    OpenOffice)](http://www.rawtherapee.com/data/RAWTherapeeManual_2.3.odt)
  - [Internal Workflow for RT v2.3 (english -
    OpenOffice)](http://www.rawtherapee.com/data/RT-Internal-Workflow_2.3.odg)

Please check before you start with a new translation in the
[Localisation
Forum](http://www.rawtherapee.com/forum/viewforum.php?f=4 "Localisation Section of the RawTherapee Forum")
if someone else already started with the same language and post there if
you start to translate something.

If you have translated the manual to a new language, please send the
result to localisation at rawtherapee dot com. This way it can be
provided here for public download.

