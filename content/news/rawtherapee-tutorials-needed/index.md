---
title: "RawTherapee Tutorials Needed"
date: 2012-07-15T17:41:00+00:00
author: dualon
Tags:
- Community
- Tutorial
---



Tutorials are important to help new users and are an easy yet rewarding
way of sharing knowledge.  
  
We opened a [dedicated forum
section](http://rawtherapee.com/forum/viewforum.php?f=15) and encourage
everyone to submit tutorials. Requirements are described in the
[instructions](http://rawtherapee.com/forum/viewtopic.php?f=15&t=4143),
please read them before posting. Finished, high quality tutorials will
be announced in the news too.  
  
Happy contribution\!  

