---
title: "PLAY RAW 44 Competition Started"
date: 2014-10-02T11:42:00+00:00
author: DrSlony
Tags:
- Community
---



Try your hand at developing a raw image by taking part in the latest
edition of our competition, PLAY RAW 44\! This round's image is from
siganmelostontos and it is an in-camera double exposure.


<figure>
<img src="playraw44_siganmelostontos_neutral_700.jpg">
</figure>


This is what the image looks like with the "Neutral" [processing
profile](http://rawpedia.rawtherapee.com/Sidecar_Files_-_Processing_Profiles "RawPedia article on Sidecar Files - Processing Profiles").


[Take part in PLAY RAW 44
now\!](http://rawtherapee.com/forum/viewtopic.php?f=11&t=5686 "Forum of PLAY RAW 44")

