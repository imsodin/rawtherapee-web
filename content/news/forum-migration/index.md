---
title: "Forum migration"
date: 2016-01-22T00:02:00+00:00
author: DrSlony
---



We have moved forums\!


In the loving and collaborative spirit of open-source, we decided to
migrate from our old phpBB forum to our new home:


[http://rawtherapee.com/forum](https://discuss.pixls.us/c/software/rawtherapee)


This brings you and us closer to the users and developers of other libre
graphics software.


Newcomers will settle right in, just be sure to first read [this
introductory
post](https://discuss.pixls.us/t/welcome-to-the-rawtherapee-forum-read-this-first/473).


Users of the old forum will want to start by reading [this
post](https://discuss.pixls.us/t/welcome-to-the-new-forum-backup-posts-from-your-old-forum/694) which
explains how you can access the old forum and what will happen with it.


Be positive, be respectful, be helpful, have fun\!

