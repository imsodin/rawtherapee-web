---
title: "RawTherapee v2.4 released"
date: 2009-07-21T14:37:00+00:00
author: dualon
draft: false
version: ""
linux_appimage: ""
windows_binary: ""
mac_binary: ""
source_code: ""
---



After a long beta cycle we are proud to announce RawTherapee v2.4\!

Changelog (from v2.3):

  - Exif/IPTC support
  - Theme switching
  - Improved GUI layout
  - Improved filebrowser
  - Ranking support
  - Basic file operations possible (delete, remove, rename)
  - Filtering based on Exif data
  - Configurable thumbnail caching
  - Live thumbnails (showing the images in the processed state)
  - Resizable thumbnails
  - Configurable thumbnail layout (left - right, top - down)
  - Basic batch processing
  - Controllable workqueue
  - Copy/Paste processing profiles (or only parts of them)
  - Direct sending of images to the workqueue
  - Improved auto-exposure and spot white balance
  - "Send to Editor" feature (Gimp, Photoshop, custom editor)

The long delay in the recent months was partially caused by the lack
of time devoted to bug fixing and by the solution of a critical bug in
shadow & highlight enhancement method.

Development did not stop at this point. The first alpha versions of v2.5
are going to appear in a month with a rearranged (and netbook-aware) GUI
and much enhanced batch processing.
