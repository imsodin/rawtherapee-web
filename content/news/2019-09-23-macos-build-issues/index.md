---
title: "macOS Build Issues"
date: 2019-09-23T01:28:59+02:00
author: DrSlony
draft: false
---

We are aware that the current macOS build of RawTherapee 5.7 (sha256sum `70f7b7f`) crashes when run on some versions of macOS. We are working on the problem.

If you experience this problem, downgrade to RawTherapee 5.6 until a new 5.7 build is ready.
