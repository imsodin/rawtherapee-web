---
title: "Can you help?"
date: 2013-08-19T21:55:00+00:00
author: DrSlony
Tags:
- Community
---



RawTherapee flourishes thanks to the hard work of the people who
continue to push their boundaries and to spend their free time
creatively and actively contributing to a wonderful global effort. You
too can join us and help make RawTherapee better while gaining
experience and learning from others. Please take a look at the list
below which details areas where we need help, and see if you could lend
a hand\!  


## Architecture & design

  - OpenCL
  - SSE
  - Expertise in finding and fixing memory leaks
  - Completion of XMP support
  - Integration with Lens correction solution(s)
  - Expertise in toolchains to make RT buildable with both GCC and VC++
  - We may also need to consider evolution of DAM functionality
  - Light weight Database catalog for fast and comprehensive inquiry
  - Reworking cache architecture (storing everything in a single
    directory is not scalable)
  - Expansion of integration options (exports, tagging, etc)
  - Smooth zooming
  - Modular engine
  - Plugin support

  

## Imaging

  - Suppression of color shifts in RGB and Lab tools
  - Localized editing
  - Faster CMS
  - Enhanced deconvolution methods, support for custom PSF
  - Soft proofing
  - Expanded partial profile library (BW options, split toning, etc)

  

## UI & Usability

  - Enhanced tooltips, integrated documentation
  - Postprocessing profile browser
  - Drag & drop UI
  - Flat curve tool
  - Threshold tool
  - Workflow management

  

## Maintenance

  - All open Google Code defects & enhancements

  

## Documentation & PR

  - Website design & maintenance
  - Comparison to other tools
  - Tutorials
  - Translation of the manual
  - Translation of the interface
  - Documentation of the source code using tools like doxygen

  
If you feel you can help with any of these, please use the [RawTherapee
developer
forums](http://rawtherapee.com/forum/viewforum.php?f=14 "RawTherapee Developer Forums").  
Documentation of RawTherapee's source code, containing a reference of
all the namespaces, classes and files, can be
found [here](http://michaelezra.com/projects/rt/documentation "Link to documentation of RawTherapee's source code.").

