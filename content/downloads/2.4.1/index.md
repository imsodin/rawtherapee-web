---
title: "RawTherapee v2.4.1"
date: 2009-09-03T08:45:00+00:00
author: dualon
draft: false
version: "2.4.1"
linux_appimage: ""
linux_binary_32: "https://rawtherapee.com/shared/builds/linux/rawtherapee241_32.tgz"
linux_binary_64: "https://rawtherapee.com/shared/builds/linux/rawtherapee241_64.tgz"
windows_binary: "https://rawtherapee.com/shared/builds/windows/rawtherapee241.exe"
windows_xp_binary: ""
mac_binary: ""
source_code: ""
---

This is a bugfix/dcraw update release.

Changelog:

- dcraw 8.97 / 1.428
- Updated translations
- Many bugs fixed
